import argparse
import sys


def parse_args(args):
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--file', type=argparse.FileType('r'), nargs='?',
                        const='text.txt', help='read text from FILE ('
                                               'default: text.txt)')
    parser.add_argument('-n', type=int, default=4, help='n in n-grams stats')
    parser.add_argument('-k', type=int, default=10, help='k in n-grams stats')
    return parser.parse_args(args)


def get_words(text):
    punctuation_marks = ('.', ',', '!', '?', ':', ';', '(', ')', '\n')
    for c in punctuation_marks:
        text = text.replace(c, ' ')
    return text.split()


def words_repeats(text):
    words_dict = {}
    for word in get_words(text):
        words_dict[word] = words_dict.get(word, 0) + 1
    for word in words_dict:
        print word + ' -> ' + str(words_dict[word])


def median(nums):
    sorted_nums = sorted(nums)
    if len(sorted_nums) % 2 != 0:
        return sorted_nums[len(sorted_nums) // 2]
    else:
        return sum(sorted_nums[len(sorted_nums) // 2 - 1:
                   len(sorted_nums) // 2 + 1]) // 2


def get_sentences(text):
    complex_punctuation_marks = ('...', '?!', '!..')
    for c in complex_punctuation_marks:
        text = text.replace(c, '.')
    ends_of_sentence = ('!', '?')
    for c in ends_of_sentence:
        text = text.replace(c, '.')
    sentences = []
    for s in text.split('.'):
        if len(s) > 0:
            sentences.append(s)
    return sentences


def sentences_stats(text):
    sentences = get_sentences(text)
    words_in_sentence = []
    if len(sentences) > 0:
        print 'Number of sentences: {0}'.format(len(sentences))
        for s in sentences:
            words_in_sentence.append(len(get_words(s)))
        print 'Average number of words per sentence: {0}'.format(sum(
            words_in_sentence) // len(sentences))
        print 'Median of numbers of words in sentences: {0}'.format(
            median(words_in_sentence))
    else:
        print 'No sentences detected.'


def occurences(string, sub):
    count = 0
    start = 0
    while True:
        start = string.find(sub, start) + 1
        if start > 0:
            count += 1
        else:
            return count


def top_k_n_grams(text, k=10, n=4):
    words = [word.lower() for word in get_words(text)]
    n_grams = {}
    for word in set(words):
        if len(word) >= n:
            for i in range(len(word) - n + 1):
                if word[i:i + n] not in n_grams:
                    n_grams[word[i:i + n]] = 0
                    for w in words:
                        n_grams[word[i:i + n]] += occurences(w, word[i:i + n])
    sorted_n_grams = sorted(n_grams.items(), key=lambda x: x[1], reverse=True)
    for i in range(k if k <= len(sorted_n_grams) else len(sorted_n_grams)):
        print '{0} -> {1}'.format(sorted_n_grams[i][0], sorted_n_grams[i][1])


def main(argv):
    args = parse_args(argv)
    if args.file is not None:
        with args.file as textfile:
            text = textfile.read()
            textfile.close()
    else:
        text = raw_input('Enter your text: ')
    words_repeats(text)
    sentences_stats(text)
    top_k_n_grams(text, args.k, args.n)

if __name__ == "__main__":
    main(sys.argv[1:])
