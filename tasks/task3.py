import re


def main():
    keys = set()
    while True:
        command = raw_input('Enter command: ').split(' ', 1)
        if len(command) >= 2:
            if command[0] == 'add':
                keys.update(command[1].split())
            elif command[0] == 'remove':
                keys.difference_update(command[1].split())
            elif command[0] == 'find':
                for x in command[1].split():
                    if x in keys:
                        print x
            elif command[0] == 'grep':
                regexp = re.compile(command[1], re.IGNORECASE)
                for key in keys:
                    if regexp.search(key):
                        print key
            elif command[0] == 'load':
                try:
                    with open(command[1], 'r') as storage:
                        keys.update(storage.read().split())
                        storage.close()
                except IOError:
                    print 'Invalid file name.'
            elif command[0] == 'save':
                try:
                    with open(command[1], 'w') as storage:
                        for key in keys:
                            storage.write(key+'\n')
                        storage.close()
                except IOError:
                    print 'Invalid file name.'
        elif (len(command) == 1) and (command[0] == 'list'):
                if len(keys) > 0:
                    for key in keys:
                        print key
                else:
                    print 'List is empty.'
        elif (len(command) == 1) and (command[0] == 'q'):
            return
        else:
            print """Unknown command.

Commands:
add KEY [KEY...] - store KEY
remove KEY [KEY...] - remove KEY
find KEY [KEY...] - find KEY
grep REGEXP - find KEYS that match REGEXP
list - list keys
load FILE - load keys from FILE
save FILE - save storage to FILE
q - quit
"""

if __name__ == "__main__":
    main()
