import argparse
import sys


def parse_args(args):
    parser = argparse.ArgumentParser()
    parser.add_argument('num', type=int, help='print num of fibonacci '
                                              'numbers')
    return parser.parse_args(args)


def fibonacci(num):
    num = int(num)
    x1, x2 = 0, 1
    for x in xrange(num):
        yield x2
        x1, x2 = x2, x1 + x2


def main(argv):
    args = parse_args(argv)
    for num in fibonacci(args.num):
        print num,

if __name__ == "__main__":
    main(sys.argv[1:])
