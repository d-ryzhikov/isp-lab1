import argparse
import math
import sys


def parse_args(args):
    parser = argparse.ArgumentParser()
    sources = parser.add_mutually_exclusive_group(required=True)
    sources.add_argument('-f', '--file', type=argparse.FileType('r'),
                         nargs='?', const='nums.txt', help='get numbers from '
                                                           'FILE (default: '
                                                           'nums.txt)')
    sources.add_argument('-i', '--inline', nargs='+', help='get numbers from '
                                                           'INLINE')
    sort_kind = parser.add_mutually_exclusive_group()
    sort_kind.add_argument('-q', '--quick', action='store_const',
                           const='q', dest='sort',
                           help='quick sort (as default)')
    sort_kind.add_argument('-m', '--merge', action='store_const',
                           const='m', dest='sort', help='merge sort')
    sort_kind.add_argument('-r', '--radix', action='store_const',
                           const='r', dest='sort', help='radix sort')
    sort_kind.set_defaults(sort='q')
    return parser.parse_args(args)


def get_nums(string):
    nums = []
    for num in string.split():
        nums.append(int(num))
    return nums


def quick_sort(nums):
    less = []
    equal = []
    greater = []
    mid = len(nums) // 2
    if len(nums) > 1:
        for x in nums:
            if x < nums[mid]:
                less.append(x)
            if x == nums[mid]:
                equal.append(x)
            if x > nums[mid]:
                greater.append(x)
        return quick_sort(less) + equal + quick_sort(greater)
    else:
        return nums


def merge_sort(nums):
    if len(nums) < 2:
        return nums
    mid = len(nums) // 2
    left = merge_sort(nums[:mid])
    right = merge_sort(nums[mid:])
    result = []
    i, j = 0, 0
    while (i < len(left)) and (j < len(right)):
        if left[i] < right[j]:
            result.append(left[i])
            i += 1
        else:
            result.append(right[j])
            j += 1
    if i < len(left):
        result.extend(left)
    if j < len(right):
        result.extend(right)
    return result


def radix_sort(nums):
    max_len = -1
    for num in nums:
        num_len = 0
        if num > 0:
            num_len = int(math.log10(num)) + 1
        if num_len > max_len:
            max_len = num_len
    buckets = [[] for _ in xrange(0, 10)]
    for digit in range(0, max_len):
        for num in nums:
            buckets[num / 10**digit % 10].append(num)
        del nums[:]
        for bucket in buckets:
            nums.extend(bucket)
            del bucket[:]
    return nums


def main(argv):
    args = parse_args(argv)
    if args.file is not None:
        with args.file as numfile:
            nums = get_nums(numfile.read())
            numfile.close()
    else:
        nums = get_nums(' '.join(args.inline))
    if args.sort == 'q':
        print quick_sort(nums)
    elif args.sort == 'm':
        print merge_sort(nums)
    elif args.sort == 'r':
        print radix_sort(nums)

if __name__ == "__main__":
    main(sys.argv[1:])
