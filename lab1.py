import argparse
from tasks import task1
from tasks import task2
from tasks import task3
from tasks import task4


def main():
    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument('task', type=int, choices=range(1, 5), help='task to '
                                                                    'execute')
    args = parser.parse_known_args()
    if args[0].task == 1:
        task1.main(args[1])
    elif args[0].task == 2:
        task2.main(args[1])
    elif args[0].task == 3:
        task3.main()
    elif args[0].task == 4:
        task4.main(args[1])

if __name__ == "__main__":
    main()
